//
//  PageThreeDetail.swift
//  UIKit Navigation
//
//  Created by Curtis Turk on 19/10/2023.
//

import SwiftUI

struct PageThreeDetail: View {
    var body: some View {
        ScrollView{
            Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id fringilla magna, ut tempus lacus. Aenean eget feugiat ex. Curabitur dapibus aliquet fermentum. Nulla rhoncus risus id enim posuere, quis dapibus purus pellentesque. Vivamus eget arcu consequat, laoreet arcu ac, elementum ligula. Donec ac aliquet urna, sit amet varius nunc. Curabitur eu erat dictum, facilisis sapien nec, convallis erat. Maecenas consectetur elit eget purus congue malesuada. Curabitur aliquet leo egestas ante viverra, vitae fermentum quam eleifend. Donec porttitor sodales nisl, vitae eleifend dui scelerisque sit amet. Vivamus vestibulum turpis vel lacus tincidunt dignissim. Phasellus ut mi et ante tristique pellentesque quis sed leo. Fusce vehicula viverra tellus. Duis scelerisque felis sit amet risus lobortis, vel lobortis ante lobortis. Donec auctor, neque auctor vulputate gravida, nisi tortor ultrices sapien, sit amet laoreet tortor risus quis dolor.").padding()
        }
        .navigationTitle("Journey 1")
//        .toolbar {
//            Button(){self.views.stacked = false}
//        label: {
//            Image(systemName: "xmark")
//        }.tint(.white)
//        }
    }
}

#Preview {
    PageThreeDetail()
}

//
//  PageOne.swift
//  UIKit Navigation
//
//  Created by Curtis Turk on 19/10/2023.
//

import SwiftUI

struct PageOne: View {
    var body: some View {
        ScrollView {
            VStack(spacing: 50) {
                Image(systemName: "house").padding(.top, 200)
                    .font(.largeTitle)
                Text("This the content for the first page")
                
                HStack{
                    Text("Page Two")
                    Image(systemName: "chevron.right")
                }
            }
        }
    }
}

#Preview {
    PageOne()
}

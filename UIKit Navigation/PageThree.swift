//
//  PageThree.swift
//  UIKit Navigation
//
//  Created by Curtis Turk on 19/10/2023.
//

import SwiftUI

struct PageThree: View {
    var body: some View {
        ScrollView{
            VStack(spacing: 50){
                Text("Third Page time").padding()
                Image(systemName: "lightbulb").font(.largeTitle)
//                NavigationLink(
//                    destination: PageThreeDetail()
//                ) {
//                    Text("Page Three")
//                }
            }
        }.navigationTitle("Page Three")
    }
}

#Preview {
    PageThree()
}

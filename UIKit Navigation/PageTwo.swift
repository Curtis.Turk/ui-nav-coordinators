//
//  PageTwo.swift
//  UIKit Navigation
//
//  Created by Curtis Turk on 19/10/2023.
//

import SwiftUI

struct PageTwo: View {
    var body: some View {
        ScrollView{
            VStack(spacing: 50){
                Text("Choose a Journey")
                    .padding(.top, 200)
                Image(systemName: "arrow.triangle.branch")
                    .rotationEffect(Angle(degrees: 180))
                    .font(.title)
//                HStack{
//                    Spacer()
//                    NavigationLink(
//                        destination: PageThree()
//                    ) {
//                        VStack {
//                            Image(systemName: "lightbulb")
//                            Text("Page Three")
//                        }
//                    }.padding()
//                    Spacer()
//                    NavigationLink(
//                        destination: PageFour()
//                    ) {
//                        VStack {
//                            Image(systemName: "snowflake")
//                            Text("Page Four")
//                        }
//                    }
//                    Spacer()
//                }
            }
        }
    }
}

#Preview {
    PageTwo()
}

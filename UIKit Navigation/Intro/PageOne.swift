//
//  PageOne.swift
//  UIKit Navigation
//
//  Created by Curtis Turk on 18/10/2023.
//

import SwiftUI

struct PageOne: View {
    
    var didTapButton: () -> Void
    
    var body: some View {
            ScrollView {
                VStack(spacing: 50) {
                    Image(systemName: "house").padding(.top, 200)
                        .font(.largeTitle)
                    Text("This the content for the first page")
                    Button("PageTwo"){
                        didTapButton()
                    }
                }
            }
    }
}

#Preview {
    PageOne {
        
    }
}
